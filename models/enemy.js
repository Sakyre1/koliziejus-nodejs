var mongoose = require('mongoose');

// Enemy Schema
var EnemySchema = mongoose.Schema({
	enemy_id: {
		type: Object,
		index: true
	},
	level: {
		type: Number
	},
	strength: {
		type: Number
	},
	perception: {
		type: Number
	},
	endurance: {
		type: Number
	},
	charisma: {
		type: Number
	},
	intelligence: {
		type: Number
	},
	agility: {
		type: Number
	},
	luck: {
		type: Number
	},
	health_points: {
		type: Number
	},
	max_health_points: {
		type: Number
	},
	stamina_points: {
		type: Number
	},
	max_stamina_points: {
		type: Number
	}
});

var Enemy = module.exports = mongoose.model('Enemy', EnemySchema);

module.exports.createEnemy = function(newEnemy, callback){
	newEnemy.save(callback);
}