var mongoose = require('mongoose');

// Sesion Schema
var SessionSchema = mongoose.Schema({
	session_id: {
		type: Object,
		index: true
	},
	//1 - coliseum, 2 - pratice, 3 - dummy;
	session_type: {
		type: Number
	},
	//true - player turn, false - bot turn
	session_turn: {
		type: Boolean
	},
	//active - true, finished - false;
	characterSessionState: {
		type: Boolean
	},
	//yes - true, no - false;
	characterDiedOrNot: {
		type: Boolean
	},
	characterHealthPoints: {
		type: Number
	},
	characterStaminaPoints: {
		type: Number
	},
	enemy_id: {
		type: Number
	},
	enemy_health: {
		type: Number
	},
	enemy_max_health: {
		type: Number
	},
	enemy_stamina: {
		type: Number
	},
	enemy_max_stamina: {
		type: Number
	},
	enemy_attack_drain: {
		type: Number
	},
	enemy_attack_hit_chance: {
		type: Number
	},
	enemy_evasion_chance: {
		type: Number
	}
});

var Session = module.exports = mongoose.model('Session', SessionSchema);

module.exports.createSession = function(newSession, callback){
	newSession.save(callback);
}