var mongoose = require('mongoose');

// Character Schema
var CharacterSchema = mongoose.Schema({
	user_id: {
		type: Object,
		index: true
	},
	//Equipped items
	characterArmoury_id: {
		type: Number
	},
	//Character inventory
	characterInventory_id: {
		type: Number
	},
	//Character coliseum match id
	characterColiseumMatch_id: {
		type: Number
	},
	//Character coliseum rank
	characterRanking_id: {
		type: Number
	},
	//Character is having a pratice match against bot
	characterInAMatch: {
		type: Boolean
	},
	//Player current points
	healthPoints: {
		type: Number
	},
	//Player maximum available health points
	maxHealthPoints: {
		type: Number
	},
	//Player current stamina points
	staminaPoints: {
		type: Number
	},
	//Player maximum available stamina points
	maxStaminaPoints: {
		type: Number
	},
	strength: {
		type: Number,
		validate : {
	    	validator : Number.isInteger,
	    	message   : '{VALUE} is not an integer value'
	  	}
	},
	perception: {
		type: Number,
		validate : {
	    	validator : Number.isInteger,
	    	message   : '{VALUE} is not an integer value'
	  	}
	},
	endurance: {
		type: Number,
		validate : {
	    	validator : Number.isInteger,
	    	message   : '{VALUE} is not an integer value'
	  	}
	},
	charisma: {
		type: Number,
		validate : {
	    	validator : Number.isInteger,
	    	message   : '{VALUE} is not an integer value'
	  	}
	},
	intelligence:{
		type: Number,
		validate : {
	    	validator : Number.isInteger,
	    	message   : '{VALUE} is not an integer value'
	  	}
	},
	agility:{
		type: Number,
		validate : {
	    	validator : Number.isInteger,
	    	message   : '{VALUE} is not an integer value'
	  	}
	},
	luck:{
		type: Number,
		validate : {
	    	validator : Number.isInteger,
	    	message   : '{VALUE} is not an integer value'
	  	}
	},
	gold: {
		type: Number
	},
	level: {
		type: Number
	},
	levelUpPoints: {
		type: Number
	},
	current_exp: {
		type: Number
	},
	required_exp_for_next_level: {
		type: Number
	},
	isAbleToLevelUp: {
		type: Boolean
	}
});

var Character = module.exports = mongoose.model('Character', CharacterSchema);


module.exports.createCharacter = function(newCharacter, callback){
	newCharacter.save(callback);
}