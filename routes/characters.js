var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://127.0.0.1:27017/koliziejus";

var Character = require('../models/character');
var Enemy = require('../models/enemy');
var Session = require('../models/session');

// Arena create dummy
router.get('/arena/dummy', ensureAuthenticated, function(req, res){
	//Put users character into data
	var data = {};
	MongoClient.connect(url, function(err0, db) {
	  if (err0) throw err0;
	  var char_id = req.user._id;
	  var query = { user_id: char_id };
	  db.collection('characters').find(query).toArray(function(err1, result) {
	    if (err1) throw err1;
	    data = result[0];
	    if(data.characterInAMatch == false){
	    	//console.log('test1');
	    	var myquery = { _id: char_id };
			var newvalues = { $set: { characterInAMatch: true } };
	    	db.collection('characters').updateOne(myquery, newvalues, function(err2, res) {
				if (err2) throw err2;
			});
			//Create a dummy enemy
			var newEnemy = new Enemy ({
				enemy_id: char_id,
				level: 0,
				strength: 0,
				perception: 0,
				endurance: 0,
				charisma: 0,
				intelligence: 0,
				agility: 0,
				luck: 0,
				health_points: 100,
				max_health_points: 100,
				stamina_points: 100,
				max_stamina_points: 100
			});

			Enemy.createEnemy(newEnemy, function(err3, enemy){
				if (err3) throw err3;
			});

			//Create new session
			var newSession = new Session ({
				session_id: char_id,
				//3 - dummy match
				session_type: 3,
				//Player starts first
				session_turn: true,
				//true - active, false - finished;
				characterSessionState: true,
				characterDiedOrNot: false,
				characterHealthPoints: 0,
				characterStaminaPoints: 0,
				enemy_id: 0,
				enemy_health: newEnemy.health_points,
				enemy_max_health: newEnemy.max_health_points,
				enemy_stamina: newEnemy.stamina_points,
				enemy_max_stamina: newEnemy.max_stamina_points,
				enemy_attack_drain: 0,
				enemy_attack_hit_chance: 0,
				enemy_evasion_chance: 0
			});

			//Update user info
			myquery = { _id: char_id };
			newvalues = { $set: { characterInSession: true } };
			db.collection("users").updateOne(myquery, newvalues, function(err4, res) {
				if (err4) throw err4;
			});
			//Create a new session
			Session.createSession(newSession, function(err5, session){
				if (err5) throw err5;
			});
			res.redirect('/characters/arena/engage');
	    	db.close();
	    }else{
	    	res.redirect('/characters/arena/engage');
	    	db.close();
	    }
	  });
	});
});

router.get('/arena/engage', ensureAuthenticated, function(req, res){
	var data = {};
	char_id = req.user._id;
	var myq = { session_id: char_id };
	var myq2 = { user_id: char_id };
	MongoClient.connect(url, function(err, db) {
		if (err) throw err;
		db.collection('characters').find(myq2).toArray(function(err1, rez1) {
			if (err1) throw err1;
			data = rez1[0];
			db.collection('sessions').find(myq).toArray(function(err2, rez2) {
				if (err2) throw err2;
				var currentSession = rez2[0];
				//Enemy percentage
				var health_percent = parseInt((currentSession.enemy_health *  100) / currentSession.enemy_max_health);
				var stamina_percent = parseInt((currentSession.enemy_stamina *  100) / currentSession.enemy_max_stamina);

				//Player percentage
				var hp = parseInt((data.healthPoints *  100) / data.maxHealthPoints);
				var sp = parseInt((data.staminaPoints *  100) / data.maxStaminaPoints);
				//res.render('arena');
				res.render('arena', {char_data: data, session: currentSession, sp: sp, hp: hp, p_hlt: health_percent, p_sta: stamina_percent});
				db.close();
			});
		});
	});
});


//Attack
router.get('/arena/attack', ensureAuthenticated, function(req, res){
	var data = {};
	var enemy_data = {};
	MongoClient.connect(url, function(err, db) {
		if (err) throw err;
		var char_id = req.user._id;
		var query = { user_id: char_id };
		db.collection('characters').find(query).toArray(function(err1, result1) {
			if (err1) throw err1;
			data = result1[0];

			//Lose stamina
			var stamina = parseInt(data.staminaPoints - 30);
			var q = { user_id: char_id };
			var v = { $set: { staminaPoints: parseInt(stamina) } };
			if(parseInt(stamina) <= 0){
				res.redirect('/characters/arena/engage');
			}else{
				//console.log(stamina);
				//console.log(data.staminaPoints);
				db.collection('characters').updateOne(q, v, function(er, re) {
					if (er) throw err;
					//Critical calculation
					var base_chance = parseInt(data.luck) * parseInt(1);
					var critical_multiplier = parseInt(parseInt(data.strength) * 0.2);
					var critical_chance = parseInt(base_chance * critical_multiplier);
					//Critical will be activated 0 - no crit, 1 - crit;
					var isCrit = 0;
					//[low, high]
					var low = 0, high = 0;
					low = parseInt(critical_chance/1.25);
					high = parseInt(critical_chance);
					var crit_dice = Math.floor(Math.random() * (high - low + 1) + low);
					var player_dice = parseInt(Math.random() * parseInt(critical_chance) + parseInt(1));
					if (player_dice >= crit_dice) {
						isCrit = 1;
					}
					//Deal damage to enemy
					var adjustedDamage = (parseInt(isCrit) * (parseInt(critical_multiplier) * parseInt(critical_chance)) + parseInt(data.strength)) + parseInt(data.strength);
					
					//Find characters current session
					var myq = { session_id: char_id };

					db.collection("sessions").find(myq).toArray(function(err2, result2) {
						if (err2) throw err2;
						enemy_data = result2[0];

						//New values for enemy
						var adjustedHealth = parseInt(enemy_data.enemy_health) - parseInt(adjustedDamage);
						//end of values

						if(adjustedHealth <= 0){
							//console.log('won!');
							db.collection("characters").find(query).toArray(function(err3, result) {
								if (err) throw err;
								data = result[0];
								var c_exp = data.current_exp + (25 * parseInt(data.level)) + (parseInt(data.charisma) * 10);
								var myquery = { user_id: char_id };
								var newvalues = { $set: { current_exp: c_exp } };
								db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
									if (error) throw error;
									res.redirect('/characters/endSession');
									db.close();
								});
							});
						}else{
							//Update
							var newvalues = { $set: { enemy_health: adjustedHealth } };

							db.collection("sessions").updateOne(myq, newvalues, function(error, resp) {
								if (error) throw error;
								res.redirect('/characters/arena/engage');
								db.close();
							});
						}
					});
				});
			}
		});
	});
});

//Defend
//Heal
router.get('/healUp', ensureAuthenticated, function(req, res){
	MongoClient.connect(url, function(err, db) {
		if (err) throw err;
		var char_id = req.user._id;
		var query = { user_id: char_id };
		db.collection("characters").find(query).toArray(function(err, result) {
			if (err) throw err;
			var data = {};
			data = result[0];
			var myquery = { user_id: char_id };
			var newvalues = { $set: { healthPoints: data.maxHealthPoints } };
			db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
				if (error) throw error;
				res.redirect('/characters/game');
			});
			db.close();
		});
	});
});


//Rest
router.get('/arena/restUp', ensureAuthenticated, function(req, res){
	MongoClient.connect(url, function(err, db) {
		if (err) throw err;
		var char_id = req.user._id;
		var query = { user_id: char_id };
		db.collection("characters").find(query).toArray(function(err, result) {
			if (err) throw err;
			var data = {};
			data = result[0];
			var myquery = { user_id: char_id };
			var newvalues = { $set: { staminaPoints: data.maxStaminaPoints } };
			db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
				if (error) throw error;
				res.redirect('/characters/arena/engage');
			});
			db.close();
		});
	});
});


//Take damage
router.get('/attackEnemy', ensureAuthenticated, function(req, res){
	MongoClient.connect(url, function(err, db) {
		if (err) throw err;
		var char_id = req.user._id;
		var query = { user_id: char_id };
		db.collection("characters").find(query).toArray(function(err, result) {
			if (err) throw err;
			var data = {};
			data = result[0];
			var health = parseInt(data.healthPoints) - parseInt(20);
			var myquery = { user_id: char_id };
			var newvalues = { $set: { healthPoints: health } };
			db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
				if (error) throw error;
				res.redirect('/characters/game');
			});
			db.close();
		});
	});
});

//Lose energy
router.get('/drainStamina', ensureAuthenticated, function(req, res){
	MongoClient.connect(url, function(err, db) {
		if (err) throw err;
		var char_id = req.user._id;
		var query = { user_id: char_id };
		db.collection("characters").find(query).toArray(function(err, result) {
			if (err) throw err;
			var data = {};
			data = result[0];
			var stmina = parseInt(data.staminaPoints) - parseInt(20);
			var myquery = { user_id: char_id };
			var newvalues = { $set: { staminaPoints: stmina } };
			db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
				if (error) throw error;
				res.redirect('/characters/game');
			});
			db.close();
		});
	});
});


// Gladiator creation web
router.get('/gladiator', ensureAuthenticated, function(req, res){
	var data = {};
	MongoClient.connect(url, function(err, db) {
		if (err) throw err;
		var char_id = req.user._id;
		var query = { _id: char_id };
		db.collection("users").find(query).toArray(function(err, result) {
			if (err) throw err;
			data = result[0];
			if(data.character_dead){
				res.redirect('/');
			}else{
				res.render('gladiator');
			}
			db.close();
		});
	});
});

// Game
router.get('/game', ensureAuthenticated, function(req, res){
	//Find users character data
	var data = {};
	MongoClient.connect(url, function(err, db) {
	  if (err) throw err;
	  var char_id = req.user._id;
	  var query = { user_id: char_id };
	  db.collection("characters").find(query).toArray(function(err, result) {
	    if (err) throw err;
	    data = result[0];
	    if(data.current_exp >= data.required_exp_for_next_level && data.isAbleToLevelUp == false){
	    	//console.log('YOU CAN LEVEL UP!');
	    	var myquery = { user_id: char_id };
	    	var isAble = true;
	    	//GIVE POINTS
	    	var givenPoints = parseInt(4 + (0.25 * parseInt(data.intelligence)) + parseInt(data.levelUpPoints));
	    	//NEW LEVEL
	    	var nextLevel = parseInt(data.level) + parseInt(1);
	    	//EXP
	    	var set_current_exp = parseInt(data.current_exp) - parseInt(data.required_exp_for_next_level);
	    	var nextExpNeeded = parseInt(data.required_exp_for_next_level) + (parseInt(nextLevel) * parseInt(100));
	    	if (set_current_exp < 0){
	    		set_current_exp = 0;
	    	}
			var newvalues = { $set: { isAbleToLevelUp: isAble, levelUpPoints: givenPoints, level: nextLevel, current_exp: set_current_exp, required_exp_for_next_level: nextExpNeeded} };
			db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
				if (error) throw error;
				//res.redirect('/characters/game');
				res.render('barracks', {char_data: data});
				db.close();
			});
		}else{
	    	res.render('barracks', {char_data: data});
	    }
	  });
	});
});

//ADD TO STATS
router.get('/addSTR', ensureAuthenticated, function(req, res){
	MongoClient.connect(url, function(err, db) {
		if (err) throw err;
		var char_id = req.user._id;
		var query = { user_id: char_id };
		db.collection("characters").find(query).toArray(function(err, result) {
			if (err) throw err;
			data = result[0];
			if(parseInt(data.levelUpPoints) > 0){
				var newStr = parseInt(data.strength) + 1;
				var updated_available_points = parseInt(data.levelUpPoints) - 1;
				var myquery = { user_id: char_id };
				if(updated_available_points <= 0){
					var isAble = false;
					var newvalues = { $set: { strength: newStr, levelUpPoints: updated_available_points, isAbleToLevelUp: isAble } };
					db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
						if (error) throw error;
						res.redirect('/characters/game');
					});
				}else{
					var newvalues = { $set: { strength: newStr, levelUpPoints: updated_available_points } };
					db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
						if (error) throw error;
						res.redirect('/characters/game');
					});
				}
				db.close();
			}else {
				console.log('test');
				db.close();
				res.redirect('/characters/game');
			}
		});
	});
});

router.get('/addPER', ensureAuthenticated, function(req, res){
	MongoClient.connect(url, function(err, db) {
		if (err) throw err;
		var char_id = req.user._id;
		var query = { user_id: char_id };
		db.collection("characters").find(query).toArray(function(err, result) {
			if (err) throw err;
			data = result[0];
			if(parseInt(data.levelUpPoints) > 0){
				var newPer = parseInt(data.perception) + 1;
				var updated_available_points = parseInt(data.levelUpPoints) - 1;
				var myquery = { user_id: char_id };
				if(updated_available_points <= 0){
					var isAble = false;
					var newvalues = { $set: { perception: newPer, levelUpPoints: updated_available_points, isAbleToLevelUp: isAble } };
					db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
						if (error) throw error;
						res.redirect('/characters/game');
					});
				}else{
					var newvalues = { $set: { perception: newPer, levelUpPoints: updated_available_points } };
					db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
						if (error) throw error;
						res.redirect('/characters/game');
					});
				}
				db.close();
			}else {
				console.log('test');
				db.close();
				res.redirect('/characters/game');
			}
		});
	});
});

router.get('/addEND', ensureAuthenticated, function(req, res){
	MongoClient.connect(url, function(err, db) {
		if (err) throw err;
		var char_id = req.user._id;
		var query = { user_id: char_id };
		db.collection("characters").find(query).toArray(function(err, result) {
			if (err) throw err;
			data = result[0];
			if(parseInt(data.levelUpPoints) > 0){
				var newEnd = parseInt(data.endurance) + 1;
				var newHealthPoints = parseInt(80 + parseInt(parseInt(data.endurance) * 5) + parseInt(2.5 + (parseInt(data.endurance) * .5) * parseInt(data.level - 1)));
				var updated_available_points = parseInt(data.levelUpPoints) - 1;
				var myquery = { user_id: char_id };
				if(updated_available_points <= 0){
					var isAble = false;
					var newvalues = { $set: { endurance: newEnd, levelUpPoints: updated_available_points, healthPoints: newHealthPoints, maxHealthPoints: newHealthPoints, isAbleToLevelUp: isAble } };
					db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
						if (error) throw error;
						res.redirect('/characters/game');
					});
				}else{
					var newvalues = { $set: { endurance: newEnd, levelUpPoints: updated_available_points, healthPoints: newHealthPoints, maxHealthPoints: newHealthPoints } };
					db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
						if (error) throw error;
						res.redirect('/characters/game');
					});
				}
				db.close();
			}else {
				console.log('test');
				db.close();
				res.redirect('/characters/game');
			}
		});
	});
});


router.get('/addCHA', ensureAuthenticated, function(req, res){
	MongoClient.connect(url, function(err, db) {
		if (err) throw err;
		var char_id = req.user._id;
		var query = { user_id: char_id };
		db.collection("characters").find(query).toArray(function(err, result) {
			if (err) throw err;
			data = result[0];
			if(parseInt(data.levelUpPoints) > 0){
				var newCha = parseInt(data.charisma) + 1;
				var updated_available_points = parseInt(data.levelUpPoints) - 1;
				var myquery = { user_id: char_id };
				if(updated_available_points <= 0){
					var isAble = false;
					var newvalues = { $set: { charisma: newCha, levelUpPoints: updated_available_points, isAbleToLevelUp: isAble } };
					db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
						if (error) throw error;
						res.redirect('/characters/game');
					});
				}else{
					var newvalues = { $set: { charisma: newCha, levelUpPoints: updated_available_points } };
					db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
						if (error) throw error;
						res.redirect('/characters/game');
					});
				}
				db.close();
			}else {
				console.log('test');
				db.close();
				res.redirect('/characters/game');
			}
		});
	});
});


router.get('/addINT', ensureAuthenticated, function(req, res){
	MongoClient.connect(url, function(err, db) {
		if (err) throw err;
		var char_id = req.user._id;
		var query = { user_id: char_id };
		db.collection("characters").find(query).toArray(function(err, result) {
			if (err) throw err;
			data = result[0];
			if(parseInt(data.levelUpPoints) > 0){
				var newInt = parseInt(data.intelligence) + 1;
				var updated_available_points = parseInt(data.levelUpPoints) - 1;
				var myquery = { user_id: char_id };
				if(updated_available_points <= 0){
					var isAble = false;
					var newvalues = { $set: { intelligence: newInt, levelUpPoints: updated_available_points, isAbleToLevelUp: isAble } };
					db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
						if (error) throw error;
						res.redirect('/characters/game');
					});
				}else{
					var newvalues = { $set: { intelligence: newInt, levelUpPoints: updated_available_points } };
					db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
						if (error) throw error;
						res.redirect('/characters/game');
					});
				}
				db.close();
			}else {
				console.log('test');
				db.close();
				res.redirect('/characters/game');
			}
		});
	});
});


router.get('/addAGI', ensureAuthenticated, function(req, res){
	MongoClient.connect(url, function(err, db) {
		if (err) throw err;
		var char_id = req.user._id;
		var query = { user_id: char_id };
		db.collection("characters").find(query).toArray(function(err, result) {
			if (err) throw err;
			data = result[0];
			if(parseInt(data.levelUpPoints) > 0){
				var newAgi = parseInt(data.agility) + 1;
				var newStaminaPoints = parseInt(80 + parseInt(parseInt(data.agility) * 5) + parseInt(2.5 + (parseInt(data.agility) * .5) * parseInt(data.level - 1)));
				var updated_available_points = parseInt(data.levelUpPoints) - 1;
				var myquery = { user_id: char_id };
				if(updated_available_points <= 0){
					var isAble = false;
					var newvalues = { $set: { agility: newAgi, levelUpPoints: updated_available_points, staminaPoints: newStaminaPoints, maxStaminaPoints: newStaminaPoints, isAbleToLevelUp: isAble } };
					db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
						if (error) throw error;
						res.redirect('/characters/game');
					});
				}else{
					var newvalues = { $set: { agility: newAgi, levelUpPoints: updated_available_points, staminaPoints: newStaminaPoints, maxStaminaPoints: newStaminaPoints } };
					db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
						if (error) throw error;
						res.redirect('/characters/game');
					});
				}
				db.close();
			}else {
				console.log('test');
				db.close();
				res.redirect('/characters/game');
			}
		});
	});
});



router.get('/addLUK', ensureAuthenticated, function(req, res){
	MongoClient.connect(url, function(err, db) {
		if (err) throw err;
		var char_id = req.user._id;
		var query = { user_id: char_id };
		db.collection("characters").find(query).toArray(function(err, result) {
			if (err) throw err;
			data = result[0];
			if(parseInt(data.levelUpPoints) > 0){
				var newLuk = parseInt(data.luck) + 1;
				var updated_available_points = parseInt(data.levelUpPoints) - 1;
				var myquery = { user_id: char_id };
				if(updated_available_points <= 0){
					var isAble = false;
					var newvalues = { $set: { luck: newLuk, levelUpPoints: updated_available_points, isAbleToLevelUp: isAble } };
					db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
						if (error) throw error;
						res.redirect('/characters/game');
					});
				}else{
					var newvalues = { $set: { luck: newLuk, levelUpPoints: updated_available_points } };
					db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
						if (error) throw error;
						res.redirect('/characters/game');
					});
				}
				db.close();
			}else {
				console.log('test');
				db.close();
				res.redirect('/characters/game');
			}
		});
	});
});


//END OF STATISTICS ADDING



// ADD EXP 
router.get('/addEXP', ensureAuthenticated, function(req, res){
	MongoClient.connect(url, function(err, db) {
		if (err) throw err;
		var char_id = req.user._id;
		var query = { user_id: char_id };
		db.collection("characters").find(query).toArray(function(err, result) {
			if (err) throw err;
			data = result[0];
			var c_exp = data.current_exp + (25 * parseInt(data.level)) + (parseInt(data.charisma) * 10);
			//console.log(data.current_exp + ' ('+ 25 + ' * ' + parseInt(data.level) + ') + (' + parseInt(data.charisma) + ' ' + '* 10' + ')');
			var myquery = { user_id: char_id };
			var newvalues = { $set: { current_exp: c_exp } };
			db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
				if (error) throw error;
				res.redirect('/characters/game');
			});
			db.close();
		});
	});
});

// RESET EXP 
router.get('/resetEXP', ensureAuthenticated, function(req, res){
	MongoClient.connect(url, function(err, db) {
		if (err) throw err;
		var char_id = req.user._id;
		var query = { user_id: char_id };
		db.collection("characters").find(query).toArray(function(err, result) {
			if (err) throw err;
			data = result[0];
			var c_exp = 0;
			var myquery = { user_id: char_id };
			var newvalues = { $set: { current_exp: c_exp, isAbleToLevelUp: false } };
			db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
				if (error) throw error;
				res.redirect('/characters/game');
			});
			db.close();
		});
	});
});





// SUICIDE
router.get('/KILL', ensureAuthenticated, function(req, res){
	var char_id = req.user._id;
	//DETELE Character
	MongoClient.connect(url, function(err, db) {
	  if (err) throw err;
	  var myquery = { user_id: char_id };
	  db.collection("characters").deleteOne(myquery, function(err, obj) {
	    if (err) throw err;
	    //console.log("1 document deleted");
	    db.close();
	  });
	});


	//UPDATE users Character information
	MongoClient.connect(url, function(err, db) {
	  if (err) throw err;
	  var myquery = { _id: char_id };
	  var newvalues = { $set: { character_dead: false } };
	  db.collection("users").updateOne(myquery, newvalues, function(err, res) {
	    if (err) throw err;
	    //console.log("1 document updated");
	    db.close();
	  });
	});

	res.redirect('/');
});


//Gladiator stats registration
router.post('/gladiator', ensureAuthenticated, function(req, res){
	var str = req.body.STR;
	var per = req.body.PER;
	var end = req.body.END;
	var cha = req.body.CHA;
	var int = req.body.INT;
	var agi = req.body.AGI;
	var luk = req.body.LUK;
	var sum = parseInt(str) + parseInt(per) + parseInt(end) + parseInt(cha) + parseInt(int) + parseInt(agi) + parseInt(luk);
	// Validation
	req.checkBody('STR', 'Strength is required').notEmpty();
	req.checkBody('PER', 'Perception is required').notEmpty();
	req.checkBody('END', 'Endurance is required').notEmpty();
	req.checkBody('CHA', 'Charisma is required').notEmpty();
	req.checkBody('INT', 'Intelligence is required').notEmpty();
	req.checkBody('AGI', 'Agility is required').notEmpty();
	req.checkBody('LUK', 'Luck is required').notEmpty();

	var errors = req.validationErrors();
	if(errors){
		res.render('gladiator',{
			errors:errors
		});
	} else {
		if(parseInt(sum) == 35 && parseInt(str) >= 0 && parseInt(per) >= 0 && parseInt(end) >= 0 && parseInt(cha) >= 0 && parseInt(int) >= 0 && parseInt(agi) >= 0 && parseInt(luk) >= 0){
			var gladiator_id = req.user._id;
			//var gladiator_id = req.user.name;
			var newCharacter = new Character({
				user_id: gladiator_id,
				characterArmoury_id: 0,
				characterInventory_id: 0,
				characterColiseumMatch_id: 0,
				characterRanking_id: 0,
				characterInAMatch: false,
				healthPoints: 80 + (parseInt(end) * 5),
				maxHealthPoints: 80 + (parseInt(end) * 5),
				staminaPoints: 80 + (parseInt(agi) * 5),
				maxStaminaPoints: 80 + (parseInt(agi) * 5),
				strength: str,
				perception: per,
				endurance: end,
				charisma: cha,
				intelligence: int,
				agility: agi,
				luck: luk,
				gold : 0,
				level: 1,
				levelUpPoints: 0,
				current_exp: 0,
				required_exp_for_next_level: 100,
				isAbleToLevelUp: false
			});

			//var char_state = req.user.character_dead;
			var char_id = req.user._id;
			//Update character state of health
			MongoClient.connect(url, function(err, db) {
			  if (err) throw err;
			  var myquery = { _id: char_id };
			  var newvalues = { $set: { character_dead: true } };
			  db.collection("users").updateOne(myquery, newvalues, function(err, res) {
			    if (err) throw err;
			    //console.log("1 document updated");
			    //console.log(newCharacter);
			    db.close();
			  });
			});

			Character.createCharacter(newCharacter, function(err, character){
				if (err) throw err;
				//console.log(character);
			});

			req.flash('success_msg', 'Your character is now created');
			//ADD in user info that the character is alive!
			res.redirect('/users/account');
		}else if(parseInt(sum) >= 35 || parseInt(sum) <= 35){
			if(parseInt(sum) > 35){
				res.render('gladiator',{points: 'You were given 35 points not ' + parseInt(sum) + '. You have not even participated in the arena and you want more that you can bargain?'});
			}else{
				res.render('gladiator',{points: 'You were given 35 points and you only spent ' + parseInt(sum) + '. Maybe you do not deserve anymore... Hmmm why did it not go through? Maybe minus points?' });
			}
		}
	}
});





//Delete current session
router.get('/endSession', ensureAuthenticated, function(req, res){
	MongoClient.connect(url, function(err0, db) {
		if (err0) throw err0;
		var char_id = req.user._id;
		var query0 = { _id: char_id };
		var query = { user_id: char_id };
		db.collection("users").find(query0).toArray(function(err, result) {
			if (err) throw err;
			var data = {};
			data = result[0];
			var myquery1 = { _id: char_id };
			var newvalues1 = { $set: { characterInAMatch: false } };
			var newvalues2 = { $set: { characterInSession: false } };
			//Update in users
			db.collection("users").updateOne(myquery1, newvalues2, function(erro, respo) {
				if (erro) throw erro;
				//console.log('characterInSession set to false');
			});
			//Update in characters
			db.collection("characters").updateOne(query, newvalues1, function(error, resp) {
				if (error) throw error;
				//console.log('characterInAMatch set to false');
			});
			var myq = { session_id: char_id };
			db.collection("sessions").deleteOne(myq, function(err, obj) {
				if (err) throw err;
				//console.log(obj);
			});
			res.redirect('/characters/game');
			db.close();
		});
	});
});




function ensureAuthenticated(req, res, next){
	if(req.isAuthenticated()){
		return next();
	} else {
		//req.flash('error_msg','You are not logged in');
		res.redirect('/users/login');
	}
}

module.exports = router;