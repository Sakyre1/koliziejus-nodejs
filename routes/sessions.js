var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://127.0.0.1:27017/koliziejus";

var Character = require('../models/character');

// Gladiator creation web
router.get('/gladiator', ensureAuthenticated, function(req, res){
	res.render('gladiator');
});

// Game
router.get('/game', ensureAuthenticated, function(req, res){
	//Find users character data
	var data = {};
	MongoClient.connect(url, function(err, db) {
	  if (err) throw err;
	  var char_id = req.user._id;
	  var query = { user_id: char_id };
	  db.collection("characters").find(query).toArray(function(err, result) {
	    if (err) throw err;
	    data = result[0];
	    if(data.current_exp >= data.required_exp_for_next_level){
	    	//console.log('YOU CAN LEVEL UP!');
	    	var myquery = { user_id: char_id };
	    	var isAble = true;
			var newvalues = { $set: { isAbleToLevelUp: isAble } };
			db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
				if (error) throw error;
				//res.redirect('/characters/game');
				res.render('barracks', {char_data: data});
				db.close();
			});
	    }else{
	    	res.render('barracks', {char_data: data});
	    }
	  });
	});
});


router.get('/level_up', ensureAuthenticated, function(req, res){
	//Find users character data
	var data = {};
	MongoClient.connect(url, function(err, db) {
	  if (err) throw err;
	  //Get user ID for the user database
	  var char_id = req.user._id;
	  var query = { user_id: char_id };
	  db.collection("characters").find(query).toArray(function(err, result) {
	    if (err) throw err;
	    data = result[0];
	    if (data.isAbleToLevelUp){
	    	var nextLevel = parseInt(data.level) + parseInt(1);
	    	var set_current_exp = parseInt(data.current_exp) - parseInt(data.required_exp_for_next_level);
	    	if (set_current_exp < 0){
	    		set_current_exp = 0;
	    	}
	    	var nextExpNeeded = parseInt(data.required_exp_for_next_level) + (parseInt(nextLevel) * parseInt(100));
	    	var myquery = { user_id: char_id };
			var newvalues = { $set: { isAbleToLevelUp: false, level: nextLevel, current_exp: set_current_exp, required_exp_for_next_level: nextExpNeeded } };
			//var newvalues = { $set: { level: nextLevel } };
			db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
				if (error) throw error;
				res.redirect('/characters/game');
				//res.render('barracks', {char_data: data});
	    		db.close();
			});
	    }
	  });
	});
});





// ADD EXP 
router.get('/addEXP', ensureAuthenticated, function(req, res){
	MongoClient.connect(url, function(err, db) {
		if (err) throw err;
		var char_id = req.user._id;
		var query = { user_id: char_id };
		db.collection("characters").find(query).toArray(function(err, result) {
			if (err) throw err;
			data = result[0];
			var c_exp = data.current_exp + 200;
			var myquery = { user_id: char_id };
			var newvalues = { $set: { current_exp: c_exp } };
			db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
				if (error) throw error;
				res.redirect('/characters/game');
			});
			db.close();
		});
	});
});

// RESET EXP 
router.get('/resetEXP', ensureAuthenticated, function(req, res){
	MongoClient.connect(url, function(err, db) {
		if (err) throw err;
		var char_id = req.user._id;
		var query = { user_id: char_id };
		db.collection("characters").find(query).toArray(function(err, result) {
			if (err) throw err;
			data = result[0];
			var c_exp = 0;
			var myquery = { user_id: char_id };
			var newvalues = { $set: { current_exp: c_exp, isAbleToLevelUp: false } };
			db.collection("characters").updateOne(myquery, newvalues, function(error, resp) {
				if (error) throw error;
				res.redirect('/characters/game');
			});
			db.close();
		});
	});
});





// SUICIDE
router.get('/KILL', ensureAuthenticated, function(req, res){
	var char_id = req.user._id;
	//DETELE Character
	MongoClient.connect(url, function(err, db) {
	  if (err) throw err;
	  var myquery = { user_id: char_id };
	  db.collection("characters").deleteOne(myquery, function(err, obj) {
	    if (err) throw err;
	    //console.log("1 document deleted");
	    db.close();
	  });
	});


	//UPDATE users Character information
	MongoClient.connect(url, function(err, db) {
	  if (err) throw err;
	  var myquery = { _id: char_id };
	  var newvalues = { $set: { character_dead: false } };
	  db.collection("users").updateOne(myquery, newvalues, function(err, res) {
	    if (err) throw err;
	    //console.log("1 document updated");
	    db.close();
	  });
	});

	res.redirect('/');
});


//TEST
/*
router.get('/AddConfirm', ensureAuthenticated, function(req, res){
	res.render('gladiator');
});
*/

//Gladiator stats registration
router.post('/gladiator', ensureAuthenticated, function(req, res){
	var str = req.body.STR;
	var per = req.body.PER;
	var end = req.body.END;
	var cha = req.body.CHA;
	var int = req.body.INT;
	var agi = req.body.AGI;
	var luk = req.body.LUK;
	var sum = parseInt(str) + parseInt(per) + parseInt(end) + parseInt(cha) + parseInt(int) + parseInt(agi) + parseInt(luk);
	// Validation
	req.checkBody('STR', 'Strength is required').notEmpty();
	req.checkBody('PER', 'Perception is required').notEmpty();
	req.checkBody('END', 'Endurance is required').notEmpty();
	req.checkBody('CHA', 'Charisma is required').notEmpty();
	req.checkBody('INT', 'Intelligence is required').notEmpty();
	req.checkBody('AGI', 'Agility is required').notEmpty();
	req.checkBody('LUK', 'Luck is required').notEmpty();

	var errors = req.validationErrors();
	if(errors){
		res.render('gladiator',{
			errors:errors
		});
	} else {
		if(parseInt(sum) == 35){
			var gladiator_id = req.user._id;
			//var gladiator_id = req.user.name;
			var newCharacter = new Character({
				user_id: gladiator_id,
				strength: str,
				perception: per,
				endurance: end,
				charisma: cha,
				intelligence: int,
				agility: agi,
				luck: luk,
				level: 1,
				current_exp: 0,
				required_exp_for_next_level: 100,
				isAbleToLevelUp: false
			});
			//var char_state = req.user.character_dead;
			var char_id = req.user._id;
			MongoClient.connect(url, function(err, db) {
			  if (err) throw err;
			  var myquery = { _id: char_id };
			  var newvalues = { $set: { character_dead: true } };
			  db.collection("users").updateOne(myquery, newvalues, function(err, res) {
			    if (err) throw err;
			    //console.log("1 document updated");
			    //console.log(newCharacter);
			    db.close();
			  });
			});

			Character.createCharacter(newCharacter, function(err, character){
				if (err) throw err;
				//console.log(character);
				//console.log('test1');
			});

			req.flash('success_msg', 'Your character is now created');
			//ADD in user info that the character is alive!
			res.redirect('/users/account');
		}else if(parseInt(sum) > 35 || parseInt(sum) < 35){
			if(parseInt(sum) > 35){
				res.render('gladiator',{points: 'You were given 35 points not ' + parseInt(sum) + '. You have not even participated in the arena and you want more that you can bargain?'});
			}else{
				res.render('gladiator',{points: 'You were given 35 points and you only spent ' + parseInt(sum) + '. Maybe you do not deserve anymore...' });
			}
			
		}
	}
});

function ensureAuthenticated(req, res, next){
	if(req.isAuthenticated()){
		return next();
	} else {
		//req.flash('error_msg','You are not logged in');
		res.redirect('/users/login');
	}
}

/* Check if character is ALIVE!
function ensureCharacterIsAlive(req, res, next){

}
*/

module.exports = router;